
#include "mem/cache/metadata/example1.hh"

#include <iostream>

#include "base/intmath.hh"
#include "cpu/base.hh"
#include "mem/cache/base.hh"
#include "params/BaseMetadata.hh"
#include "params/Example1Metadata.hh"
#include "sim/system.hh"

using namespace std;

namespace Metadata {

Example1::Example1(const Example1MetadataParams *p)
    : BaseMeta(p), granularity(p->granularity), size(p->size)
{
    cout<<name()<<" granularity:"<<granularity<<" size:"<<size<<endl;
    blkSize=64;
    startMDI = false;
}

Example1::~Example1()
{
    
}

void
Example1::pktCheck(PacketPtr pkt)
{
    if(!startMDI)
        return;
    
    Addr adrr = base + (pkt->getAddr()%length);
    RequestPtr translation_req = createMetaRequest(adrr, pkt);
    if(translation_req->hasContextId() && startMDI){
        DeferredPacket dpp(this, 10000);
        dpp.setTranslationRequest(translation_req);
        dpp.tc = cache->system->threads[translation_req->contextId()];
        dpp.pkt=pkt;
        assert(tlb);
        dpp.startTranslation(tlb); 
    }
}

void
Example1::translationComplete(DeferredPacket *dp, bool failed,
                            const RequestPtr &req, ThreadContext *tc)
{
    if(!failed){
        handleMetadata(req, dp->pkt);
    }
}

void
Example1::init(vector<Addr> params)
{
    cout<<"initializing the interface!"<<endl;
    if(params.size()==2){
        base = params[0];
        length = params[1];
    }
    cout<<"base: "<<hex<<base<<" length:"<<length<<endl;
}

void
Example1::start()
{
    startMDI = true;
}

void
Example1::finish()
{
    startMDI = false;
}


PacketPtr 
Example1::createMetaPacket(Addr paddr, PacketPtr pkt)
{
    int requestorId = 0;
    RequestPtr req = std::make_shared<Request>(
            paddr, 64, pkt->req->getFlags(), requestorId);
    // RequestPtr req = createMetaRequest(paddr, pkt);
    req->taskId(ContextSwitchTaskId::Metadata);
    PacketPtr pktMeta = new Packet(req, MemCmd::MetaReq);
    pktMeta->req->setPC(pkt->req->getPC());
    // if (pkt->hasSharers() && !pkt->needsWritable()) {
        // pkt->setHasSharers();
    // }
    pktMeta->allocate();
    return pktMeta;
}

bool 
Example1::handleMetadata(const RequestPtr &req, PacketPtr pkt)
{
    Addr addr = req->getPaddr();
    PacketPtr metaPkt = createMetaPacket(blockAddress(addr), pkt);
    Cycles lat1(5);
    CacheBlk *blkMeta = nullptr;
    Cycles forwardLatency(10);
    bool satisfiedMeta = false; 
    {
        PacketList writebacksMeta;
        satisfiedMeta = cache->access(metaPkt, blkMeta, lat1, writebacksMeta);
        cache->doWritebacks(writebacksMeta, clockEdge(lat1 + forwardLatency));
    }
    
    if(satisfiedMeta){
        uint8_t *data = blkMeta->data;
        uint64_t u64;
        memcpy(&u64, data, sizeof u64);
        u64+=1;
        memcpy(data, &u64, sizeof u64);
        blkMeta->status |= BlkDirty;
        delete metaPkt;
    }else{
        Tick forward_time = clockEdge(forwardLatency) + metaPkt->headerDelay;
        Tick request_time = clockEdge(lat1);
        sendMetaPacket(metaPkt, blkMeta, forward_time, request_time); 
    }
    return satisfiedMeta;
}

void 
Example1::sendMetaPacket(PacketPtr pkt, CacheBlk *blk,
                               Tick forward_time, Tick request_time)
{
    Addr blk_addr = pkt->getBlockAddr(blkSize);
	MSHR *mshr = cache->mshrQueue.findMatch(blk_addr, pkt->isSecure());
	if (mshr) {
		if (pkt) {
			cache->stats.cmdStats(pkt).mshr_hits[pkt->req->requestorId()]++;
            mshr->allocateTarget(pkt, forward_time, cache->order++,
                                     cache->allocOnFill(pkt->cmd));
			if (mshr->getNumTargets() == cache->numTarget) {
				cache->noTargetMSHR = mshr;
				cache->setBlocked(cache->Blocked_NoTargets);
			}
		}
	}else{
		cache->stats.cmdStats(pkt).mshr_misses[pkt->req->requestorId()]++;
        if (blk && blk->isValid()) {
		  blk->status &= ~BlkReadable;
		}
        cache->allocateMissBuffer(pkt, forward_time, false);
    }
	
}


Addr
Example1::blockAddress(Addr a)
{
    return a & ~((Addr)blkSize-1);
}
} // namespace Metadata

Metadata::Example1*
Example1MetadataParams::create()
{
   return new Metadata::Example1(this);
}
