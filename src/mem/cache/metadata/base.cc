/*
 * Copyright (c) 2013-2014 ARM Limited
 * All rights reserved.
 *
 * The license below extends only to copyright in the software and shall
 * not be construed as granting a license to any other intellectual
 * property including but not limited to intellectual property relating
 * to a hardware implementation of the functionality of the software
 * licensed hereunder.  You may use the software subject to the license
 * terms below provided that you ensure that this notice is replicated
 * unmodified and in its entirety in all distributions of the software,
 * modified or unmodified, in source code or in binary form.
 *
 * Copyright (c) 2005 The Regents of The University of Michigan
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * @file
 * Hardware Metadata Definition.
 */

#include "mem/cache/metadata/base.hh"
#include <cassert>
#include "base/intmath.hh"
#include "cpu/base.hh"
#include "mem/cache/base.hh"
#include "params/BaseMetadata.hh"
#include "sim/system.hh"

using namespace std;

namespace Metadata {

BaseMeta::BaseMeta(const BaseMetadataParams *p)
    : ClockedObject(p), cache(nullptr), blkSize(p->block_size),
      lBlkSize(floorLog2(blkSize)),
      useVirtualAddresses(p->use_virtual_addresses),
      tlb(p->tlb)
{
}

void
BaseMeta::DeferredPacket::createPkt(Addr paddr, unsigned blk_size,
                                            RequestorID requestor_id,
                                            Tick t) {
}

void
BaseMeta::DeferredPacket::startTranslation(BaseTLB *tlb)
{
    assert(translationRequest != nullptr);
    if (!ongoingTranslation) {
        ongoingTranslation = true;
        tlb->translateTiming(translationRequest, tc, this, BaseTLB::Read);
    }
}
 
void
BaseMeta::DeferredPacket::finish(const Fault &fault,
    const RequestPtr &req, ThreadContext *tc, BaseTLB::Mode mode)
{
    assert(ongoingTranslation);
    ongoingTranslation = false;
    bool failed = (fault != NoFault);
    owner->translationComplete(this, failed, 
                                req, tc);
}

void
BaseMeta::translationComplete(DeferredPacket *dp, bool failed,
                            const RequestPtr &req, ThreadContext *tc)
{
    // cout<<"BaseMeta::translationComplete "<<failed<<endl;
}

void 
BaseMeta::init(vector<Addr> params)
{
    
}

void
BaseMeta::start()
{
    
}

void
BaseMeta::finish()
{
    
}

void
BaseMeta::setCache(BaseCache *_cache)
{
    assert(!cache);
    cache = _cache;

    // If the cache has a different block size from the system's, save it
    blkSize = cache->getBlockSize();
    lBlkSize = floorLog2(blkSize);
    //Majid:TODO
    //I literally do not know why I am doing this
    //ridiculous thing. I could have defined the client inside 
    //header of this :(
    cache->getClient()->addClient(this);
}


void
BaseMeta::probeNotify(const PacketPtr &pkt, bool miss)
{
    // Don't notify prefetcher on SWPrefetch, cache maintenance
    // operations or for writes that we are coaslescing.
    if (pkt->cmd.isSWPrefetch()) return;
    if (pkt->req->isCacheMaintenance()) return;
    if (pkt->isWrite() && cache != nullptr && cache->coalesce()) return;
    if (!pkt->req->hasPaddr()) {
        panic("Request must have a physical address");
    }
    // Verify this access type is observed by prefetcher
    if (observeAccess(pkt, miss)) {
        pktCheck(pkt);
    }
}

void
BaseMeta::MetadataListener::notify(const PacketPtr &pkt)
{
    parent.probeNotify(pkt, true);
}

void
BaseMeta::pktCheck(PacketPtr pkt)
{
    cout<<"BaseMeta::pktCheck"<<endl;
}


RequestPtr 
BaseMeta::createMetaRequest(Addr addr, PacketPtr pkt)
{
    // cout<<"BaseMeta::createMetaRequest "<<addr<<" "<<pkt->print()<<endl;
    int requestorId = 0;
    RequestPtr translation_req = std::make_shared<Request>(
            addr, 64, pkt->req->getFlags(), requestorId, 0,
            0);
            
    //Request(Addr paddr, unsigned size, Flags flags, RequestorID id) 
    translation_req->setFlags(Request::METADATA);
    return translation_req;
}

void
BaseMeta::regProbeListeners()
{
    /**
     * If no probes were added by the configuration scripts, connect to the
     * parent cache using the probe "Miss". Also connect to "Hit", if the
     * cache is configured to prefetch on accesses.
     */
    if (listeners.empty() && cache != nullptr) {
        cout<<"adding three listeners"<<endl;
        ProbeManager *pm(cache->getProbeManager());
        listeners.push_back(new MetadataListener(*this, pm, "Miss", false,
                                                true));
        listeners.push_back(new MetadataListener(*this, pm, "Fill", true,
                                                 false));
        listeners.push_back(new MetadataListener(*this, pm, "Hit", false,
                                                     false));

    }
}

void
BaseMeta::addEventProbe(SimObject *obj, const char *name)
{
    cout<<"BaseMeta::addEventProbe"<<endl;
    ProbeManager *pm(obj->getProbeManager());
    listeners.push_back(new MetadataListener(*this, pm, name));
}

void
BaseMeta::addTLB(BaseTLB *t)
{
    fatal_if(tlb != nullptr, "Only one TLB can be registered");
    tlb = t;
}

bool
BaseMeta::observeAccess(const PacketPtr &pkt, bool miss) const
{
    // bool fetch = pkt->req->isInstFetch();
    // bool read = pkt->isRead();
    // bool inv = pkt->isInvalidate();

    // if (pkt->req->isUncacheable()) return false;
    // if (fetch && !onInst) return false;
    // if (!fetch && !onData) return false;
    // if (!fetch && read && !onRead) return false;
    // if (!fetch && !read && !onWrite) return false;
    // if (!fetch && !read && inv) return false;
    // if (pkt->cmd == MemCmd::WriteReq) return true;


    // if (onMiss) {
        // return miss;
    // }

    return true;
}


} // namespace Metadata
