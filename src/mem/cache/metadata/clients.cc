#include "mem/cache/metadata/clients.hh"

#include <unistd.h>

#include <cmath>
#include <iostream>
#include <string>
#include <vector>

#include "params/BaseMetadata.hh"
#include "params/ClientsMetadata.hh"

namespace Metadata {
Clients::Clients(const ClientsMetadataParams *params) :
    ClockedObject(params)
{
    std::cout << "Hello World! From a SimObject! "<< std::endl;
}

Clients::~Clients()
{

}

void
Clients::onExit()
{
    std::cout<<"onExit"<<std::endl;
}

void
Clients::init()
{

}

void
Clients::addClient(BaseMeta* c)
{
    if (c){
        cout<<name()<<" "<<"adding a new client "<<c->name();
        ClientList.push_back(c);
        cout<<" ClientList size: "<<ClientList.size()<<endl;
    }else{
         cout<<"cache is null"<<endl;
    }
}

void
Clients::printClients(){
    for (int i = 0 ; i < ClientList.size(); i++){
        cout<<i<<") "<<ClientList[i]->name()<<endl;
    }
}

string
Clients::location(Addr a,  bool is_secure)
{
    return "";
}

int
Clients::locationNum(Addr PC, Addr a, bool is_secure)
{
        return 0;
}

BaseMeta*
Clients::returnClient(int idx)
{
    if (idx<ClientList.size()){
        return ClientList[idx];
    }
    return nullptr;
}
}// namespace Metadata

Metadata::Clients*
ClientsMetadataParams::create()
{
    cout<<"******Created"<<endl;
    static  Metadata::Clients aa(this);
    return &aa;
}










