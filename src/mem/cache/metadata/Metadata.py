# Copyright (c) 2012, 2014, 2019 ARM Limited
# All rights reserved.
#
# The license below extends only to copyright in the software and shall
# not be construed as granting a license to any other intellectual
# property including but not limited to intellectual property relating
# to a hardware implementation of the functionality of the software
# licensed hereunder.  You may use the software subject to the license
# terms below provided that you ensure that this notice is replicated
# unmodified and in its entirety in all distributions of the software,
# modified or unmodified, in source code or in binary form.
#
# Copyright (c) 2005 The Regents of The University of Michigan
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met: redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer;
# redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution;
# neither the name of the copyright holders nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

from m5.SimObject import *
from m5.params import *
from m5.proxy import *

from m5.objects.ClockedObject import ClockedObject


###Majid
# from BaseTLB import BaseTLB
# from X86TLB import X86TLB
from m5.objects.X86TLB import X86TLB

class METAProbeEvent(object):
    def __init__(self, metadata, obj, *listOfNames):
        self.obj = obj
        self.metadata = metadata
        self.names = listOfNames

    def register(self):
        if self.obj:
            for name in self.names:
                self.metadata.getCCObject().addEventProbe(
                    self.obj.getCCObject(), name)

class BaseMetadata(ClockedObject):
    type = 'BaseMetadata'
    abstract = True
    cxx_class = 'Metadata::BaseMeta'
    cxx_header = "mem/cache/metadata/base.hh"
    cxx_exports = [
        PyBindMethod("addEventProbe"),
        PyBindMethod("addTLB"),
    ]
    sys = Param.System(Parent.any, "System this metadata belongs to")

    # Get the block size from the parent (system)
    block_size = Param.Int(Parent.cache_line_size, "Block size in bytes")

    use_virtual_addresses = Param.Bool(False,
        "Use virtual addresses for prefetching")
    tlb = Param.X86TLB(X86TLB(), "TLB/MMU to walk page table")
    
    def __init__(self, **kwargs):
        super(BaseMetadata, self).__init__(**kwargs)
        self._events = []
        self._tlbs = []

    def addEvent(self, newObject):
        self._events.append(newObject)

    # Override the normal SimObject::regProbeListeners method and
    # register deferred event handlers.
    def regProbeListeners(self):
        for tlb in self._tlbs:
            self.getCCObject().addTLB(tlb.getCCObject())
        for event in self._events:
            event.register()
        self.getCCObject().regProbeListeners()

    def listenFromProbe(self, simObj, *probeNames):
        if not isinstance(simObj, SimObject):
            raise TypeError("argument must be of SimObject type")
        if len(probeNames) <= 0:
            raise TypeError("probeNames must have at least one element")
        self.addEvent(METAProbeEvent(self, simObj, *probeNames))


    def registerTLB(self, simObj):
        if not isinstance(simObj, SimObject):
            raise TypeError("argument must be a SimObject type")
        self._tlbs.append(simObj)


class Example1Metadata(BaseMetadata):
    type = 'Example1Metadata'
    cxx_class = 'Metadata::Example1'
    cxx_header = "mem/cache/metadata/example1.hh"

    granularity = Param.Int(2, "Granularity ")
    size = Param.Int(16, "Size")
    
# class ClientsMetadata(ClockedObject):
    # type = 'ClientsMetadata'
    # abstract = True
    # cxx_class = 'Metadata::Clients'
    # cxx_header = "mem/cache/metadata/clients.hh"
    # sys = Param.System(Parent.any, "System this metadata belongs to")

    # Get the block size from the parent (system)
    # block_size = Param.Int(Parent.cache_line_size, "Block size in bytes")
    # def __init__(self, **kwargs):
        # print("ClientsMetadata created")
        # super(ClientsMetadata, self).__init__(**kwargs)

class ClientsMetadata(ClockedObject):
    type = 'ClientsMetadata'
    cxx_class = 'Metadata::Clients'
    cxx_header = "mem/cache/metadata/clients.hh"

