#ifndef __MEM_CACHE_METADATA_CLIENTS_HH__
#define __MEM_CACHE_METADATA_CLIENTS_HH__

// #include "params/Clients.hh"
#include <cassert>
#include <cmath>
#include <cstdint>
#include <string>
#include <vector>

#include "mem/cache/metadata/base.hh"
#include "sim/clocked_object.hh"

struct ClientsMetadataParams;
namespace Metadata{
class Clients: public ClockedObject
{
        private:
                std::vector<BaseMeta *> ClientList;
                std::vector<int> numList;

        public:
                Clients(const ClientsMetadataParams *params);
        ~Clients();
        void onExit();
        void addClient(BaseMeta* c);
        void printClients();
        void init();
        string location(Addr a, bool is_secure);
        int locationNum(Addr PC, Addr a,  bool is_secure);
                BaseMeta*  returnClient(int idx);
};
}
#endif //__MEM_CACHE_METADAT_CLIENTS_HH__
