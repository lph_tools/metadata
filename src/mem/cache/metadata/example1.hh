
#ifndef __MEM_CACHE_METADATA_EXAMPLE1_HH__
#define __MEM_CACHE_METADATA_EXAMPLE1_HH__

#include <vector>

#include "mem/cache/metadata/base.hh"
#include "mem/cache/cache_blk.hh"
#include "mem/packet.hh"
#include "mem/cache/mshr.hh"

struct Example1MetadataParams;
using namespace std;
namespace Metadata {

class Example1 : public BaseMeta
{
  protected:
      const int granularity;
      const int size;
      Addr base;
      Addr length;
      bool startMDI;
      int blkSize;
       

  public:
    Example1(const Example1MetadataParams *p);
    ~Example1();
    virtual void pktCheck(PacketPtr pkt);
    virtual void translationComplete(DeferredPacket *dp, bool failed,
                            const RequestPtr &req, ThreadContext *tc);

    virtual void init(vector<Addr> params);
    virtual void start();
    virtual void finish();
    PacketPtr createMetaPacket(Addr paddr, PacketPtr pkt);
    bool handleMetadata(const RequestPtr &req, PacketPtr pkt);
    void sendMetaPacket(PacketPtr pkt, CacheBlk *blk,
                               Tick forward_time, Tick request_time);
    Addr blockAddress(Addr a);
};

} // namespace Metadata

#endif // __MEM_CACHE_METADATA_EXAMPLE1_HH__
