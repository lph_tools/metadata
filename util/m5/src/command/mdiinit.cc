#include <iostream>

#include "args.hh"
#include "command.hh"
#include "dispatch_table.hh"

namespace
{


bool
do_mdiinit(const DispatchTable &dt, Args &args)
{
    // void m5_mdi_init(uint64_t mdiNumber, uint64_t base, uint64_t size);
    uint64_t a, b, c;
    if (!args.pop(a) || !args.pop(b) || !args.pop(c, 0))
        return false;

    (*dt.m5_mdi_init)(a, b, c);
    // std::cout << "do_mdiinit " << sum << "." << std::endl;

    return true;
}

Command init = {
    "init", 2, 6, do_mdiinit, "<a> <b> <c>\n"
        "        Init (the metadata interface" };

} // anonymous namespace
