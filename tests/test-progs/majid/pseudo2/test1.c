#include <stdio.h>
#include <gem5/m5ops.h>
#include <stdint.h>
#include "LibMDI.h"
#include <string.h>
#include <ctype.h>

int main(){
    
    size_t size = 2;
    init_MDI("Dcache-Counter", size);
    start_MDI("Dcache-Counter");
    
    double f = 0;
    for(int i = 0 ; i < 1000; i++){
        f++;
    }
    finish_MDI("Dcache-Counter");
    return 0;	
}