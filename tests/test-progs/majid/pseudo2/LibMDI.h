#include <stdio.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>
#include <gem5/m5ops.h>
#include <stdint.h>
#include <ctype.h>
#include <inttypes.h>
#include <stdlib.h>

char * baseAddr = 0;
int init_MDI(char* name, size_t size){
    size_t pagesize = getpagesize();

    printf("System page size: %zu bytes\n", pagesize);
    
    char * region = mmap(
            0,   // Map from the start of the 2^20th page
            size,                         // for one page length
            PROT_READ|PROT_WRITE|PROT_EXEC,
            MAP_ANON|MAP_PRIVATE,             // to a private block of hardware memory
            0,
            0
            );
    if (region == MAP_FAILED) {
        perror("Could not mmap");
        return 1;
    }
    baseAddr=region;
    printf("addr of region: %p\n", region);
    uint64_t u64;
    memcpy(&u64, baseAddr, sizeof u64);
    printf("Hit:%" PRIu64 "\n", u64);
    
    memset(region, 0, sizeof(region));
    m5_mdi_init(0, (uint64_t)baseAddr, size);
    return 0;
}

int start_MDI(char* name){
    m5_mdi_start(0);
    return 0;
}

int finish_MDI(char* name){
    m5_mdi_finish(0);
    printf("addr of region: %p\n", baseAddr);
    uint64_t u64;
    memcpy(&u64, baseAddr, sizeof u64);
    printf("Hit:%" PRIu64 "\n", u64);
    return 0;
}









